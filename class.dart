class App {
  String? name;
  String? sector;
  String? developer;
  int? year;

  void printAppInformation() {
    name = name?.toUpperCase();
    print("Name of the app is $name");
    print("The sector/category of the app is $sector");
    print("The developer of the app is $developer");
    print("The app was developed in $year");
  }
}

void main(List<String> args) {
  var Shyft = new App();
  Shyft.name = "Shyft";
  Shyft.sector = "Online Banking";
  Shyft.developer = "Starndard Bank";
  Shyft.year = 2021;

  Shyft.printAppInformation();
}
